pub mod lib {
    use std::collections::HashMap;

    pub enum Method {
        Post,
        Get,
        Put,
        Delete,
    }

    #[allow(dead_code)]
    pub struct Request {
        method: Method,
        uri: String,
        headers: HashMap<String, String>,
        body: Vec<u8>,
    }

    impl Request {
        pub fn builder() -> RequestBuilder<(), (), Headers, ()> {
            RequestBuilder((), (), HashMap::new(), ())
        }

        pub fn get(uri: impl Into<String>) -> RequestBuilder<Method, Uri, Headers, ()> {
            RequestBuilder(Method::Get, uri.into(), HashMap::new(), ())
        }

        pub fn post(uri: impl Into<String>) -> RequestBuilder<Method, Uri, Headers, ()> {
            RequestBuilder(Method::Post, uri.into(), HashMap::new(), ())
        }
    }

    pub struct RequestBuilder<M, U, H, B>(M, U, H, B);
    type Uri = String;
    type Headers = HashMap<String, String>;
    type Body = Vec<u8>;

    impl<U, H, B> RequestBuilder<(), U, H, B> {
        pub fn method(self, method: Method) -> RequestBuilder<Method, U, H, B> {
            RequestBuilder(method, self.1, self.2, self.3)
        }
    }

    impl<M, H, B> RequestBuilder<M, (), H, B> {
        pub fn uri(self, uri: impl Into<String>) -> RequestBuilder<M, Uri, H, B> {
            RequestBuilder(self.0, uri.into(), self.2, self.3)
        }
    }

    impl<M, U, B> RequestBuilder<M, U, Headers, B> {
        pub fn with_header(
            self,
            key: impl Into<String>,
            value: impl Into<String>,
        ) -> RequestBuilder<M, U, Headers, B> {
            let mut headers = self.2;
            headers.insert(key.into(), value.into());
            RequestBuilder(self.0, self.1, headers, self.3)
        }
    }

    impl<M, U, H> RequestBuilder<M, U, H, ()> {
        pub fn append_body(self, bytes: &[u8]) -> RequestBuilder<M, U, H, Body> {
            RequestBuilder(self.0, self.1, self.2, bytes.to_vec())
        }
    }

    impl<M, U, H> RequestBuilder<M, U, H, Body> {
        pub fn append_body(self, bytes: &[u8]) -> RequestBuilder<M, U, H, Body> {
            let mut body = self.3;
            body.extend_from_slice(bytes);
            RequestBuilder(self.0, self.1, self.2, body)
        }
    }

    impl RequestBuilder<Method, Uri, Headers, Body> {
        pub fn build(self) -> Request {
            Request {
                method: self.0,
                uri: self.1,
                headers: self.2,
                body: self.3,
            }
        }
    }
}

#[cfg(test)]
mod test {
    use super::lib::*;
    use crate::compile_fail;

    #[test]
    fn it_works() {
        let _ = Request::builder()
            .method(Method::Get)
            .uri("127.0.0.1:8000")
            .with_header("Authorization", "Bearer cafe-babe-abad-1dea")
            .with_header("Content-Type", "application/json")
            .append_body(r#"{"foo": "bar"}"#.as_bytes())
            .build();

        // Factory pattern variant
        let _ = Request::get("127.0.0.1:8000")
            .with_header("Authorization", "Bearer cafe-babe-abad-1dea")
            .with_header("Content-Type", "application/json")
            .append_body(r#"{"foo": "bar"}"#.as_bytes())
            .build();

        // Unordered initialization
        let _ = Request::builder()
            .append_body(r#"{"foo": "bar"}"#.as_bytes())
            .uri("127.0.0.1:8000")
            .with_header("Authorization", "Bearer cafe-babe-abad-1dea")
            .method(Method::Get)
            .with_header("Content-Type", "application/json")
            .build();

        compile_fail! {
            let _ = Request::builder().build();
            let _ = Request::builder().method(Method::Get).method(Method::Get);
        }
    }
}
