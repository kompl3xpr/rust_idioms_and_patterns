pub mod lib {
    use std::collections::HashMap;

    pub enum Method {
        Post,
        Get,
        Put,
        Delete,
    }

    #[allow(dead_code)]
    pub struct Request {
        method: Method,
        uri: String,
        headers: HashMap<String, String>,
        body: Vec<u8>,
    }

    impl Request {
        pub fn builder() -> RequestBuilder<()> {
            RequestBuilder(())
        }

        pub fn get(uri: impl Into<String>) -> RequestBuilder<(Method, Uri, Headers)> {
            RequestBuilder((Method::Get, uri.into(), HashMap::new()))
        }

        pub fn post(uri: impl Into<String>) -> RequestBuilder<(Method, Uri, Headers)> {
            RequestBuilder((Method::Post, uri.into(), HashMap::new()))
        }
    }

    pub struct RequestBuilder<S>(S);
    type Uri = String;
    type Headers = HashMap<String, String>;
    type Body = Vec<u8>;

    impl RequestBuilder<()> {
        pub fn method(self, method: Method) -> RequestBuilder<(Method,)> {
            RequestBuilder((method,))
        }
    }

    impl RequestBuilder<(Method,)> {
        pub fn uri(self, uri: impl Into<String>) -> RequestBuilder<(Method, Uri, Headers)> {
            let (method,) = self.0;
            RequestBuilder((method, uri.into(), HashMap::new()))
        }
    }

    impl RequestBuilder<(Method, Uri, Headers)> {
        pub fn with_header(mut self, key: impl Into<String>, value: impl Into<String>) -> Self {
            self.0 .2.insert(key.into(), value.into());
            self
        }

        pub fn append_body(self, bytes: &[u8]) -> RequestBuilder<(Method, Uri, Headers, Body)> {
            let (method, uri, headers) = self.0;
            RequestBuilder((method, uri, headers, bytes.to_vec()))
        }
    }

    impl RequestBuilder<(Method, Uri, Headers, Body)> {
        pub fn append_body(mut self, bytes: &[u8]) -> Self {
            self.0 .3.extend_from_slice(bytes);
            self
        }

        pub fn build(self) -> Request {
            let (method, uri, headers, body) = self.0;
            Request {
                method,
                uri,
                headers,
                body,
            }
        }
    }
}

#[cfg(test)]
mod test {
    use super::lib::*;
    use crate::compile_fail;

    #[test]
    fn it_works() {
        let _ = Request::builder()
            .method(Method::Get)
            .uri("127.0.0.1:8000")
            .with_header("Authorization", "Bearer cafe-babe-abad-1dea")
            .with_header("Content-Type", "application/json")
            .append_body(r#"{"foo": "bar"}"#.as_bytes())
            .build();

        // Factory pattern variant
        let _ = Request::get("127.0.0.1:8000")
            .with_header("Authorization", "Bearer cafe-babe-abad-1dea")
            .with_header("Content-Type", "application/json")
            .append_body(r#"{"foo": "bar"}"#.as_bytes())
            .build();

        compile_fail! {
            // Compile-time error occurs when any field is uninitialized:
            let _ = Request::builder().build();
            let _ = Request::builder().method(Method::Get).method(Method::Get);

            // Unordered initialization also causes a compile-time error:
            let _ = Request::builder()
                .append_body(r#"{"foo": "bar"}"#.as_bytes())
                .uri("127.0.0.1:8000")
                .with_header("Authorization", "Bearer cafe-babe-abad-1dea")
                .method(Method::Get)
                .with_header("Content-Type", "application/json")
                .build();
        }
    }
}
