pub mod lib {
    use std::collections::HashMap;

    #[derive(Clone, Copy, Debug, PartialEq, Eq)]
    pub enum Method {
        Post,
        Get,
        Put,
        Delete,
        // ...
    }

    #[allow(dead_code)]
    #[derive(Clone, Debug, PartialEq, Eq)]
    pub struct Request {
        method: Method,
        uri: String,
        headers: HashMap<String, String>,
        body: Vec<u8>,
    }

    impl Request {
        pub fn builder() -> RequestBuilder {
            RequestBuilder::default()
        }

        pub fn get(uri: impl Into<String>) -> RequestBuilder {
            RequestBuilder {
                method: Some(Method::Get),
                uri: Some(uri.into()),
                ..Default::default()
            }
        }

        pub fn post(uri: impl Into<String>) -> RequestBuilder {
            RequestBuilder {
                method: Some(Method::Post),
                uri: Some(uri.into()),
                ..Default::default()
            }
        }
    }

    #[derive(Default)]
    pub struct RequestBuilder {
        method: Option<Method>,
        uri: Option<String>,
        header: HashMap<String, String>,
        body: Vec<u8>,
    }

    impl RequestBuilder {
        pub fn method(mut self, method: Method) -> Self {
            self.method = Some(method);
            self
        }

        pub fn uri(mut self, uri: impl Into<String>) -> Self {
            self.uri = Some(uri.into());
            self
        }

        pub fn with_header(mut self, key: impl Into<String>, value: impl Into<String>) -> Self {
            self.header.insert(key.into(), value.into());
            self
        }

        pub fn append_body(mut self, bytes: &[u8]) -> Self {
            self.body.extend_from_slice(bytes);
            self
        }

        pub fn build(self) -> Request {
            Request {
                method: self.method.unwrap(),
                uri: self.uri.unwrap(),
                headers: self.header,
                body: self.body,
            }
        }
    }

    #[derive(Debug, Clone, Copy, PartialEq, Eq)]
    pub enum BuilderError {
        UninitMethod,
        UninitUri,
    }

    impl RequestBuilder {
        pub fn try_build(self) -> Result<Request, BuilderError> {
            Ok(Request {
                method: self.method.ok_or(BuilderError::UninitMethod)?,
                uri: self.uri.ok_or(BuilderError::UninitUri)?,
                headers: self.header,
                body: self.body,
            })
        }
    }
}

#[cfg(test)]
mod test {
    use super::lib::*;

    #[test]
    fn it_works() {
        let _ = Request::builder()
            .method(Method::Get)
            .uri("127.0.0.1:8000")
            .with_header("Authorization", "Bearer cafe-babe-abad-1dea")
            .with_header("Content-Type", "application/json")
            .append_body(r#"{"foo": "bar"}"#.as_bytes())
            .build();

        // Factory pattern variant
        let _ = Request::get("127.0.0.1:8000")
            .with_header("Authorization", "Bearer cafe-babe-abad-1dea")
            .with_header("Content-Type", "application/json")
            .append_body(r#"{"foo": "bar"}"#.as_bytes())
            .build();

        // Unordered initialization
        let _ = Request::builder()
            .append_body(r#"{"foo": "bar"}"#.as_bytes())
            .uri("127.0.0.1:8000")
            .with_header("Authorization", "Bearer cafe-babe-abad-1dea")
            .method(Method::Get)
            .with_header("Content-Type", "application/json")
            .build();

        let result = Request::builder()
            .uri("127.0.0.1:8000")
            .append_body(&[])
            .try_build();
        assert_eq!(result, Err(BuilderError::UninitMethod));

        // init at multiple times is allowed.
        let _ = Request::builder().method(Method::Get).method(Method::Get);
    }
}
