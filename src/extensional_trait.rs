pub mod lib {
    pub trait Movable {
        // to be implemented
        fn move_foward(&mut self);
        fn turn_left(&mut self);
        fn turn_right(&mut self);

        // which is allowed to be overrided
        fn turn_back(&mut self) {
            self.turn_left();
            self.turn_left();
        }
    }

    pub trait MovableExt: Movable {
        // which is NOT allowed to be overrided
        fn dance(&mut self, count: usize) {
            for _ in 0..count {
                self.move_foward();
                self.turn_right();
            }
        }
    }

    impl<T: Movable> MovableExt for T {}
}

#[cfg(test)]
mod test {
    use crate::compile_fail;

    use super::lib::{Movable, MovableExt};
    #[derive(Clone, Copy, PartialEq, Eq, Debug)]
    enum Direction {
        Left,
        Right,
        Up,
        Down,
    }
    impl Direction {
        fn rrotate(self) -> Self {
            match self {
                Direction::Left => Direction::Up,
                Direction::Right => Direction::Down,
                Direction::Up => Direction::Right,
                Direction::Down => Self::Left,
            }
        }

        fn reverse(self) -> Self {
            self.rrotate().rrotate()
        }

        fn lrotate(self) -> Self {
            self.reverse().rrotate()
        }
    }

    #[derive(Clone, Copy, PartialEq, Eq, Debug)]
    struct Turtle(i32, i32, Direction);

    impl Movable for Turtle {
        fn move_foward(&mut self) {
            match self.2 {
                Direction::Left => self.1 -= 1,
                Direction::Right => self.1 += 1,
                Direction::Up => self.0 -= 1,
                Direction::Down => self.0 += 1,
            }
        }

        fn turn_left(&mut self) {
            self.2 = self.2.lrotate();
        }

        fn turn_right(&mut self) {
            self.2 = self.2.rrotate();
        }

        fn turn_back(&mut self) {
            self.2 = self.2.reverse();
        }
    }

    compile_fail! {
        impl MovableExt for Turtle {
            fn dance(&mut self, count: usize) {
                for _ in 0..count {
                    self.move_foward
                }
            }
        }
    }

    #[test]
    fn it_works() {
        let mut turtle = Turtle(0, 0, Direction::Up);
        turtle.dance(30);
        assert_eq!(turtle, Turtle(-1, 1, Direction::Down));
    }
}
