pub mod lib {
    pub struct Calculator<S: Typestate> {
        data: i32,
        _marker: PhantomData<S>,
    }

    mod states {
        pub struct Add;
        pub struct Sub;
        pub struct End;

        pub trait Typestate {}
        impl Typestate for Add {}
        impl Typestate for Sub {}
        impl Typestate for End {}
    }
    use std::marker::PhantomData;

    use states::*;

    impl<S: Typestate> Calculator<S> {
        #[inline]
        fn transit<NewState: Typestate>(self) -> Calculator<NewState> {
            Calculator {
                data: self.data,
                _marker: PhantomData,
            }
        }
    }

    impl Default for Calculator<Add> {
        fn default() -> Self {
            Self::new()
        }
    }

    impl Calculator<Add> {
        pub fn new() -> Self {
            Self {
                data: 0,
                _marker: PhantomData,
            }
        }

        pub fn ok(self) -> Calculator<End> {
            self.transit()
        }
    }

    impl std::ops::Add<i32> for Calculator<Add> {
        type Output = Calculator<Sub>;
        fn add(mut self, rhs: i32) -> Self::Output {
            self.data += rhs;
            self.transit()
        }
    }

    impl std::ops::Sub<i32> for Calculator<Sub> {
        type Output = Calculator<Add>;
        fn sub(mut self, rhs: i32) -> Self::Output {
            self.data -= rhs;
            self.transit()
        }
    }

    impl Calculator<End> {
        pub fn into_inner(self) -> i32 {
            self.data
        }
    }
}

#[cfg(test)]
mod test {
    use crate::compile_fail;

    use super::lib::*;

    #[test]
    fn it_works() {
        // State diagram:
        //      ^ := ::new -> Add
        //    Add := ::sub -> Sub | ::ok -> End
        //    Sub := ::add -> Add
        //    End := ::into_inner -> $
        let data = (Calculator::new() + 3 - 4 + 1 - 2).ok();
        assert_eq!(data.into_inner(), -2);

        compile_fail! {
            let data = (Calculator::new() + 3 + 1 - 4 - 2).ok();
            let data = (Calculator::new() + 3 - 2 - 4 + 1).ok();
        }
    }
}
