pub mod lib {
    // callers are NOT able to implement `EvenOrOdd` for any other types
    pub trait EvenOrOdd: private::Sealed {
        // callers cannot call this function
        fn unchecked_add_one(&mut self, _: private::PrivateToken);

        fn neg(&mut self);
    }

    #[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
    pub struct Even(pub i32);
    impl Even {
        pub fn new(n: i32) -> Option<Self> {
            (n & 1 == 0).then_some(Even(n))
        }
    }
    impl EvenOrOdd for Even {
        fn unchecked_add_one(&mut self, _: private::PrivateToken) {
            self.0 += 1;
        }

        fn neg(&mut self) {
            self.0 = -self.0
        }
    }

    #[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
    pub struct Odd(pub i32);
    impl Odd {
        pub fn new(n: i32) -> Option<Self> {
            (n & 1 == 1).then_some(Odd(n))
        }
    }
    impl EvenOrOdd for Odd {
        fn unchecked_add_one(&mut self, _: private::PrivateToken) {
            self.0 += 1;
        }
        fn neg(&mut self) {
            self.0 = -self.0
        }
    }

    pub fn add_two(t: &mut impl EvenOrOdd) {
        t.unchecked_add_one(private::PrivateToken);
        t.unchecked_add_one(private::PrivateToken);
    }

    mod private {
        pub trait Sealed {}
        impl Sealed for super::Even {}
        impl Sealed for super::Odd {}

        pub struct PrivateToken;
    }
}

#[cfg(test)]
mod test {
    use super::lib::*;
    use crate::compile_fail;

    #[test]
    fn it_works() {
        let mut n = Even::new(2).unwrap();
        add_two(&mut n);
        n.neg();

        // it's impossible to call `unchecked_add_one`:
        compile_fail! {
            n.unchecked_add_one(/* ??? */);
        }
        assert_eq!(n, Even(-4));

        compile_fail! {
            struct MyNat(i32);
            impl EvenOrOdd for MyNat {
                fn unchecked_add_one(&mut self, _: private::PrivateToken) {
                    self.0 += 1;
                }

                fn neg(&mut self) {
                    self.0 = -self.0
                }
            }
        }
    }
}
