pub mod builder;
pub mod extensional_trait;
pub mod magic_param_handler;
pub mod sealed_trait;
pub mod typestate;
pub mod oop;

#[macro_export]
macro_rules! compile_fail {
    ($($tokens: tt)*) => {};
}
