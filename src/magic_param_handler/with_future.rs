macro_rules! impl_fut {
    ($lt: lifetime + $($type:tt)+) => {
        impl std::future::Future<Output = $($type)+> + Send + $lt
    };
    ($lt: lifetime) => {
        impl std::future::Future<Output = ()> + Send + $lt
    };
    ($($type:tt)+) => {
        impl std::future::Future<Output = $($type)+> + Send
    };
    () => {
        impl std::future::Future<Output = ()> + Send
    };
}

mod lib {
    use std::{future::Future, sync::Arc};

    use tokio::sync::RwLock;

    #[derive(Default)]
    pub struct Context {
        foo: Arc<RwLock<Vec<u8>>>,
        bar: Arc<String>,
    }

    pub trait Param: Send + Sized {
        fn from_context(cx: &Context) -> impl std::future::Future<Output = Self> + Send;
    }

    mod params {
        use super::{Context, Param};
        use std::sync::Arc;
        use tokio::sync::RwLock;

        pub struct Foo(pub Arc<RwLock<Vec<u8>>>);
        impl Param for Foo {
            fn from_context(cx: &Context) -> impl_fut!(Self) {
                async { Foo(cx.foo.clone()) }
            }
        }

        pub struct Bar(pub Arc<String>);
        impl Param for Bar {
            fn from_context(cx: &Context) -> impl_fut!(Self) {
                async { Bar(cx.bar.clone()) }
            }
        }
    }

    pub use params::*;

    pub trait FnHandler<Args> {
        fn handle(&self, cx: &Context) -> impl_fut!();
    }

    macro_rules! impl_fn_handler {
        ($($args: ident),*) => {
            impl<$($args,)* Func, Fut> FnHandler<($($args,)*)> for Func
            where
                $($args: Param,)*
                Func: Fn($($args),*) -> Fut + Sync,
                Fut: Future<Output = ()> + Send,
            {
                fn handle(&self, _cx: &Context) -> impl_fut!() {
                    async { self(
                        $($args::from_context(_cx).await,)*
                    ).await }
                }
            }
        };
    }
    impl_fn_handler!();
    impl_fn_handler!(A);
    impl_fn_handler!(A, B);
}

#[cfg(test)]
mod test {
    use std::marker::PhantomData;

    use super::lib::*;

    #[tokio::test]
    async fn it_works() {
        async fn push_byte(Foo(foo): Foo, Bar(bar): Bar) {
            let mut foo = foo.write().await;
            foo.push(if bar.len() % 2 == 0 { 0x00 } else { 0xff });
        }

        let executor = Executor::new(Context::default())
            .with_hander(|| async {})
            .with_hander(push_byte)
            .with_hander(|Foo(foo): Foo| async move {
                if foo.read().await.last().copied() == Some(0x00) {
                    println!("end with `0x00`!")
                }
            });

        executor.run().await;
    }

    pub struct Executor<A, H> {
        handlers: H,
        cx: Context,
        _marker: PhantomData<A>,
    }

    impl Executor<(), ()> {
        fn new(cx: Context) -> Self {
            Self {
                handlers: (),
                cx,
                _marker: PhantomData,
            }
        }
    }

    impl<A1, H1: FnHandlerGroup<A1>> Executor<A1, H1> {
        fn with_hander<A2, H2>(self, h: H2) -> Executor<(A1, A2), (H1, H2)>
        where
            H2: FnHandler<A2>,
        {
            Executor {
                handlers: (self.handlers, h),
                cx: self.cx,
                _marker: PhantomData,
            }
        }

        async fn run(&self) {
            self.handlers.handle_all(&self.cx).await
        }
    }

    pub trait FnHandlerGroup<Args> {
        fn handle_all(&self, cx: &Context) -> impl_fut!();
    }

    impl FnHandlerGroup<()> for () {
        fn handle_all(&self, _: &Context) -> impl_fut!() {
            async {}
        }
    }

    impl<A1, A2, H1, H2> FnHandlerGroup<(A1, A2)> for (H1, H2)
    where
        (H1, H2): Sync,
        H1: FnHandlerGroup<A1>,
        H2: FnHandler<A2>,
    {
        fn handle_all(&self, cx: &Context) -> impl_fut!() {
            async {
                tokio::join!(self.0.handle_all(cx), self.1.handle(cx),);
            }
        }
    }
}
