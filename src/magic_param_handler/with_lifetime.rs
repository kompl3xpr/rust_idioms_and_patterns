pub mod lib {
    use std::cell::RefCell;

    pub struct Context {
        pub foo: Vec<usize>,
        pub bar: RefCell<bool>,
        pub baz: bool,
    }

    pub trait Handler {
        fn handle(&mut self, cx: &Context);
    }

    pub trait Param {
        type This<'x>;

        fn from_context<'x>(cx: &'x Context) -> Self::This<'x>;
    }

    mod params {
        use super::{Context, Param};

        pub struct Foo<'a>(pub &'a [usize]);
        impl<'a> Param for Foo<'a> {
            type This<'x> = Foo<'x>;

            fn from_context<'x>(cx: &'x Context) -> Self::This<'x> {
                Foo(&cx.foo[..])
            }
        }

        pub struct ReadBar(pub bool);
        impl Param for ReadBar {
            type This<'x> = ReadBar;

            fn from_context<'x>(cx: &'x Context) -> Self::This<'x> {
                ReadBar(*cx.bar.borrow())
            }
        }

        use std::cell::RefMut;
        pub struct WriteBar<'a>(pub RefMut<'a, bool>);
        impl<'a> Param for WriteBar<'a> {
            type This<'x> = WriteBar<'x>;

            fn from_context<'x>(cx: &'x Context) -> Self::This<'x> {
                WriteBar(cx.bar.borrow_mut())
            }
        }

        pub struct ReadBaz(pub bool);
        impl Param for ReadBaz {
            type This<'x> = ReadBaz;

            fn from_context<'x>(cx: &'x Context) -> Self::This<'x> {
                ReadBaz(cx.baz)
            }
        }
    }

    pub use params::*;

    pub trait FnHandler<Args> {
        fn call(&mut self, cx: &Context);
    }

    pub trait FnHandlerExt<Args>: FnHandler<Args> {
        fn boxed(self) -> Box<dyn Handler>;
    }

    impl<T> Handler for Box<dyn FnHandler<T>> {
        fn handle(&mut self, cx: &Context) {
            self.call(cx)
        }
    }

    macro_rules! impl_fn_handler {
        ($($args: ident),*) => {
            impl<$($args,)* Func> FnHandler<($($args,)*)> for Func
            where
                $($args: Param,)*
                Func: for<'a> FnMut($($args::This<'a>),*),
            {
                fn call(&mut self, _cx: &Context) {
                    self($($args::from_context(_cx),)*)
                }
            }

            impl<$($args,)* Func> FnHandlerExt<($($args,)*)> for Func
            where
                Func: FnMut($($args),*) + FnHandler<($($args,)*)> + 'static,
                Box<dyn FnHandler<($($args,)*)>>: 'static,
            {
                fn boxed(self) -> Box<dyn Handler> {
                    Box::new(Box::new(self) as Box<dyn FnHandler<($($args,)*)>>)
                }
            }
        };
    }

    impl_fn_handler!();
    impl_fn_handler!(A);
    impl_fn_handler!(A, B);
    impl_fn_handler!(A, B, C);
    impl_fn_handler!(A, B, C, D);
}

#[cfg(test)]
mod test {
    use super::lib::*;
    use std::cell::RefCell;

    #[test]
    fn it_works() {
        let context = Context {
            foo: vec![1, 2, 3],
            bar: RefCell::new(false),
            baz: true,
        };

        let mut executor = Executor::new(context)
            .with_handler(|baz: ReadBaz, bar: ReadBar| assert_eq!(baz.0, !bar.0))
            .with_handler(|WriteBar(mut bar): WriteBar| *bar = !*bar)
            .with_handler(|Foo(foo1): Foo, Foo(foo2): Foo| assert_eq!(foo1, foo2))
            .with_handler(|ReadBar(bar): ReadBar, ReadBaz(baz): ReadBaz| assert_eq!(baz, bar))
            .with_handler(|_: Foo, _: ReadBaz, _: ReadBar| ());

        executor.run();
    }

    struct Executor {
        handlers: Vec<Box<dyn Handler>>,
        context: Context,
    }
    impl Executor {
        fn new(context: Context) -> Self {
            Self {
                handlers: vec![],
                context,
            }
        }

        fn with_handler<Args, H: FnHandlerExt<Args>>(mut self, handler: H) -> Self {
            self.handlers.push(handler.boxed());
            self
        }

        fn run(&mut self) {
            for handler in &mut self.handlers {
                handler.handle(&self.context);
            }
        }
    }
}
