pub mod simple;
pub mod with_future;

#[allow(clippy::needless_lifetimes)]
pub mod with_lifetime;
