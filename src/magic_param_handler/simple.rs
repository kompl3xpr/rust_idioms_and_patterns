pub mod lib {
    pub struct Context {
        pub foo: bool,
        pub bar: bool,
    }

    pub trait Handler {
        fn handle(&mut self, cx: &Context);
    }

    pub trait Param {
        fn from_context(cx: &Context) -> Self;
    }

    mod params {
        use super::{Context, Param};
        pub struct Foo(pub bool);
        impl Param for Foo {
            fn from_context(cx: &Context) -> Self {
                Foo(cx.foo)
            }
        }

        pub struct Bar(pub bool);
        impl Param for Bar {
            fn from_context(cx: &Context) -> Self {
                Bar(cx.bar)
            }
        }
    }
    pub use params::*;

    pub trait FnHandler<Args> {
        fn call(&mut self, cx: &Context);
    }

    pub trait FnHandlerExt<Args>: FnHandler<Args> {
        fn boxed(self) -> Box<dyn Handler>;
    }

    impl<T> Handler for Box<dyn FnHandler<T>> {
        fn handle(&mut self, cx: &Context) {
            self.call(cx)
        }
    }

    macro_rules! impl_fn_handler {
        ($($args: ident),*) => {
            impl<$($args,)* Func> FnHandler<($($args,)*)> for Func
            where
                $($args: Param,)*
                Func: FnMut($($args),*),
            {
                fn call(&mut self, _cx: &Context) {
                    self($($args::from_context(_cx),)*)
                }
            }

            impl<$($args,)* Func> FnHandlerExt<($($args,)*)> for Func
            where
                Func: FnHandler<($($args,)*)> + 'static,
                Box<dyn FnHandler<($($args,)*)>>: 'static,
            {
                fn boxed(self) -> Box<dyn Handler> {
                    Box::new(Box::new(self) as Box<dyn FnHandler<($($args,)*)>>)
                }
            }
        };
    }

    impl_fn_handler!();
    impl_fn_handler!(A);
    impl_fn_handler!(A, B);
    impl_fn_handler!(A, B, C);
    impl_fn_handler!(A, B, C, D);
}

#[cfg(test)]
mod test {
    use super::lib::*;

    #[test]
    fn it_works() {
        let context = Context {
            foo: true,
            bar: false,
        };

        let mut executor = Executor::new(context)
            .with_handler(|| ())
            .with_handler(|Foo(f): Foo, Bar(b): Bar| assert_eq!(f, !b))
            .with_handler(|b1: Bar, b2: Bar| assert_eq!(b1.0, b2.0))
            .with_handler(|Foo(foo): Foo| assert!(foo));

        executor.run();
    }
    struct Executor {
        handlers: Vec<Box<dyn Handler>>,
        context: Context,
    }
    impl Executor {
        fn new(context: Context) -> Self {
            Self {
                handlers: vec![],
                context,
            }
        }

        fn with_handler<Args, H: FnHandlerExt<Args>>(mut self, handler: H) -> Self {
            self.handlers.push(handler.boxed());
            self
        }

        fn run(&mut self) {
            for handler in &mut self.handlers {
                handler.handle(&self.context);
            }
        }
    }
}
