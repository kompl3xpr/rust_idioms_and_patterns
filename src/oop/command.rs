pub mod lib {
    #[derive(Default)]
    pub struct Context(pub String);

    pub trait Command {
        fn run(&self, cx: &mut Context);
        fn rollback(&self, cx: &mut Context);
    }

    #[derive(Default)]
    pub struct Executor {
        redo_stack: Vec<Box<dyn Command>>,
        undo_stack: Vec<Box<dyn Command>>,
        pub context: Context,
    }

    impl Executor {
        pub fn exec(&mut self, cmd: impl Command + 'static) {
            cmd.run(&mut self.context);
            self.undo_stack.push(Box::new(cmd));
        }

        pub fn undo(&mut self) -> bool {
            let Some(cmd) = self.undo_stack.pop() else {
                return false;
            };
            cmd.rollback(&mut self.context);
            self.redo_stack.push(cmd);
            true
        }

        pub fn redo(&mut self) -> bool {
            let Some(cmd) = self.redo_stack.pop() else {
                return false;
            };
            cmd.run(&mut self.context);
            self.undo_stack.push(cmd);
            true
        }
    }
}

#[cfg(test)]
mod test {
    use super::lib::*;

    #[test]
    fn it_works() {
        let mut e = Executor::default();
        e.exec(PushChar('H'));
        e.exec(PushChar('e'));
        e.exec(PushChar('l'));
        e.undo();
        e.redo();
        e.exec(PushChar('l'));
        e.exec(PushChar('o'));
        e.undo();
        e.undo();
        e.redo();
        e.exec(PushChar('!'));
        assert_eq!(e.context.0, "Hell!");
    }

    struct PushChar(char);
    impl Command for PushChar {
        fn run(&self, cx: &mut Context) {
            cx.0.push(self.0)
        }

        fn rollback(&self, cx: &mut Context) {
            cx.0.pop();
        }
    }
}
