pub mod observer;
pub mod command;
pub mod strategy;
pub mod factory;
pub mod visitor;
pub mod decorator;