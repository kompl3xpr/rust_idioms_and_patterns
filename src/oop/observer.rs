pub mod lib {
    use std::{collections::VecDeque, future::Future, time::Duration};
    use tokio::{sync::RwLock, time::sleep};

    #[derive(Clone, Copy, Debug)]
    pub enum Event {
        AddAsset,
        OneLoaded,
    }

    pub type Asset = String;
    pub type BoxedFut<T> = std::pin::Pin<Box<dyn Future<Output = T> + Send>>;

    #[derive(Default)]
    pub struct Loader {
        observers: Vec<Box<dyn Fn(Event) -> BoxedFut<()>>>,
        assets: RwLock<VecDeque<Asset>>,
    }

    impl Loader {
        pub fn new() -> Self {
            Self::default()
        }

        pub fn with_observer<O>(mut self, observer: O) -> Self
        where
            O: Fn(Event) -> BoxedFut<()> + 'static,
        {
            self.observers.push(Box::new(observer));
            self
        }

        fn notify(&self, ev: Event) {
            for observer in &self.observers {
                tokio::spawn(observer(ev));
            }
        }

        pub async fn add_asset(&self, a: Asset) {
            self.assets.write().await.push_back(a);
            self.notify(Event::AddAsset);
        }

        pub async fn load_all_assets(&self) {
            while !self.assets.read().await.is_empty() {
                let _asset = { self.assets.write().await.pop_front().unwrap() };
                sleep(Duration::from_millis(10)).await;
                self.notify(Event::OneLoaded);
            }
        }
    }
}

#[cfg(test)]
mod test {
    use std::time::Duration;

    use tokio::{sync::Mutex, time::sleep};
    use super::lib::*;

    #[tokio::test]
    async fn it_works() {
        #[derive(Default)]
        struct Progress {
            total: usize,
            loaded: usize,
        }
        impl Progress {
            fn show(&self) {
                let percent = self.loaded as f64 / self.total as f64 * 100.0;
                println!("Progress: {percent:.2}% ({}/{})", self.loaded, self.total);
            }
        }

        let progress = &*Box::leak(Box::new(Mutex::new(Progress::default())));
        let loader = Loader::new().with_observer(move |ev: Event| {
            Box::pin(async move {
                let mut progress = progress.lock().await;
                match ev {
                    Event::AddAsset => progress.total += 1,
                    Event::OneLoaded => {
                        progress.loaded += 1;
                        progress.show();
                    }
                }
            })
        });

        loader.add_asset("a".into()).await;
        loader.add_asset("b".into()).await;
        loader.add_asset("c".into()).await;
        loader.load_all_assets().await;

        sleep(Duration::from_millis(300)).await;
    }
}
