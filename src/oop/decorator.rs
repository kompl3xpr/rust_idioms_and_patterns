pub mod lib {
    pub fn decrypter() {}
    pub trait Decrypt: Sized {
        fn decrypt(&mut self, ciphertext: &[u8]) -> Vec<u8>;
    }

    impl<D: Decrypt> DecryptExt for D {}
    pub trait DecryptExt: Decrypt {
        fn caesar(self, offset: i8) -> Caesar<Self> {
            Caesar {
                inner: self,
                offset,
            }
        }

        fn reverse(self) -> Reverse<Self> {
            Reverse(self)
        }

        fn swap(self) -> Swap<Self> {
            Swap(self)
        }
    }

    impl Decrypt for () {
        fn decrypt(&mut self, ciphertext: &[u8]) -> Vec<u8> {
            ciphertext.to_vec()
        }
    }

    pub struct Caesar<D> {
        inner: D,
        offset: i8,
    }
    impl<D: Decrypt> Decrypt for Caesar<D> {
        fn decrypt(&mut self, ciphertext: &[u8]) -> Vec<u8> {
            let mut result = self.inner.decrypt(ciphertext);
            let d = |b: &mut u8, c| *b = (((*b - c) as i8 + self.offset + 26) % 26) as u8 + c;
            for byte in &mut result {
                match *byte {
                    b'a'..=b'z' => d(byte, b'a'),
                    b'A'..=b'Z' => d(byte, b'A'),
                    _ => (),
                }
            }
            result
        }
    }

    pub struct Reverse<D>(D);
    impl<D: Decrypt> Decrypt for Reverse<D> {
        fn decrypt(&mut self, ciphertext: &[u8]) -> Vec<u8> {
            let mut result = self.0.decrypt(ciphertext);
            result.reverse();
            result
        }
    }

    pub struct Swap<D>(D);
    impl<D: Decrypt> Decrypt for Swap<D> {
        fn decrypt(&mut self, ciphertext: &[u8]) -> Vec<u8> {
            let mut result = self.0.decrypt(ciphertext);
            let len = result.len() & !1_usize;
            for chunk in result[..len].chunks_mut(2) {
                let (l, r) = chunk.split_at_mut(1);
                std::mem::swap(&mut l[0], &mut r[0]);
            }
            result
        }
    }
}

#[cfg(test)]
mod test {
    use super::lib::*;

    #[test]
    fn it_works() {
        let mut de = decrypter()
            .caesar(-3)
            .reverse()
            .caesar(4)
            .swap();

        assert_eq!(de.decrypt(b"Hello, World!"), b"e!smXp, mpfmI",)
    }
}
